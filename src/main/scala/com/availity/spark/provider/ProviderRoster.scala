package com.availity.spark.provider

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{ concat_ws, date_format, month}

object ProviderRoster  {

  def main(args: Array[String]): Unit = {
    // Create Spark Session
    val spark = SparkSession.builder()
      .appName("ProviderReport")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._

    // Load Providers data
    val providerDF = spark.read.option("header","true")
      .option("delimiter","|")
      .csv("data/providers.csv")
      .select($"provider_id",concat_ws(" ",$"first_name",$"middle_name",$"last_name").alias("full_name"),$"provider_specialty".as("provider_speciality"))

    //Load Visits data
    val visitsDF = spark.read.option("header","false") //No Header provided
      .option("delimiter",",")
      .csv("data/visits.csv")
      .toDF("visit_id","provider_id","date_of_service") //provided custom column names

    //Problem1: calculate the total number of visits per provider, partitioned  by speciality
    val totalVisitsPerProvider = visitsDF.groupBy($"provider_id").count
    val providerReport1 = providerDF.join(totalVisitsPerProvider, Seq("provider_id"), "left")
      .select($"provider_id",$"full_name",$"provider_speciality",$"count".as("total_visits"))

    providerReport1.write.partitionBy("provider_speciality").json("output/totalVisitsPerProviderReport")
    //Problem2: calculate the total number of visits per provider per month
    val visitsPerMonth =  visitsDF.withColumn("month_name",date_format($"date_of_service", "MMMM"))
      .withColumn("month", month($"date_of_service"))
      .groupBy($"provider_id",$"month_name",$"month").count()

    val providerReport2 = visitsPerMonth.join(providerDF, Seq("provider_id"), "left")
      .select($"provider_id",$"month_name",$"month",$"count".as("total_visits"))
      .orderBy($"provider_id",$"month")
      .drop("month")

    providerReport2.write.json("output/visitsPerMonthReport")

    //Stop SparkSession
    spark.stop()
  }
}